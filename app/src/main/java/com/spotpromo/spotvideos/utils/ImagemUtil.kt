package com.spotpromo.spotvideos.utils

import android.content.Context
import android.net.Uri
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*

object ImagemUtil {
    @Throws(java.lang.Exception::class)
    fun retornaCaminhoFotoURINew(
        uriAtual: Uri?,
        codRoteiro: Int,
        nomefoto: String?,
        context: Context
    ): String? {
        var retorna_novo_caminho_ou_antigo = ""
        val sdcard = context.getExternalFilesDir(null)

        /** PASTA PROJETO  */
        val pasta_projeto = File(
            sdcard!!.absolutePath + File.separator + "SPOT_FOTO"
        )
        if (!pasta_projeto.isDirectory) pasta_projeto.mkdirs()
        /** PASTA SPOT FOTOS  */
        val pasta_spot = File(
            pasta_projeto.absolutePath + File.separator + "SPOT_FOTO"
        )
        if (!pasta_spot.isDirectory) pasta_spot.mkdirs()
        val pasta_roteiro =
            File(pasta_spot.absolutePath + File.separator + codRoteiro)
        if (!pasta_roteiro.isDirectory) pasta_roteiro.mkdirs()
        val stringi_file_copy: String = CriarFileTemporarioStringRoteiro(
            pasta_roteiro.absolutePath,
            nomefoto!!,
            codRoteiro
        )
        val file_copy = File(stringi_file_copy)

        /** COPIAR ARQUIVO PARA NOVA PASTA  */
        var `in`: InputStream? = null
        var out: OutputStream? = null
        try {
            `in` = context.contentResolver.openInputStream(uriAtual!!)
            out = FileOutputStream(file_copy)

            val buf = ByteArray(1024)
            var len: Int
            while (`in`!!.read(buf).also { len = it } > 0) {
                out.write(buf, 0, len)
            }

            if (file_copy.isFile) retorna_novo_caminho_ou_antigo = file_copy.absolutePath
        } finally {
            `in`?.close()
            out?.close()
        }

        val file_movido = File(retorna_novo_caminho_ou_antigo)
        if (!file_movido.isFile)
            throw Exception("A foto não foi atualizada, por favor tente novamente")

        return retorna_novo_caminho_ou_antigo

    }

    @Throws(java.lang.Exception::class)
    fun CriarFileTemporarioStringRoteiro(
        diretorio: String,
        nomefoto: String,
        codRoteiro: Int
    ): String {
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(Date())
        val mediaFile: File
        mediaFile =
            File(diretorio + File.separator + nomefoto + "_" + codRoteiro + "_" + timeStamp + ".jpg")
        return mediaFile.absolutePath
    }
}