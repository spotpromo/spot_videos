package com.spotpromo.spotvideos.utils

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View

class RecycleItemDecoration
constructor(space : Int) : RecyclerView.ItemDecoration(){

    val space : Int = space

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.left = space
        outRect.right = space
        outRect.bottom = space

        if(parent.getChildLayoutPosition(view) == 0)
            outRect.top = space
        else
            outRect.top = 0
    }
}