package com.spotpromo.spotvideos.playvideo

import android.net.Uri
import android.os.Bundle
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import com.spotpromo.spotvideos.R
import kotlinx.android.synthetic.main.layout_spot_video_play.*

class SpotVideoPlay : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_spot_video_play)
        setUpVideoPlay()
    }

    private fun setUpVideoPlay() {
        val bundle = intent.extras
        val videoURL = bundle!!.getString("url")

        vvPlayVideo.setMediaController( MediaController(this))
        vvPlayVideo.setVideoURI(Uri.parse(videoURL))
        vvPlayVideo.requestFocus()
        vvPlayVideo.start()
    }

    override fun onBackPressed() {
        finish()
    }
}