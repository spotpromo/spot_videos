package com.spotpromo.spotvideos.config

object SpotVideoConfig {
    var buttonColor = "#263552"
    var photoSize = "1024x768"
    var tempFilePath = "temp_file.mp4"
    var tempFileCompressed = "temp_file.mp4"
    var timeLimit = 30
}