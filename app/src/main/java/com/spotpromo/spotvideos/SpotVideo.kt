package com.spotpromo.spotvideos

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.*
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.abedelazizshe.lightcompressorlibrary.CompressionListener
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor
import com.abedelazizshe.lightcompressorlibrary.VideoQuality
import com.google.common.util.concurrent.ListenableFuture
import com.spotpromo.spotvideos.config.SpotVideoConfig
import com.spotpromo.spotvideos.playvideo.SpotVideoPlay
import com.spotpromo.spotvideos.utils.Alerta
import kotlinx.android.synthetic.main.layout_spot_video.*
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


open class SpotVideo : AppCompatActivity(){
    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var cameraExecutor: ExecutorService
    private var videoCapture: VideoCapture? = null
    private val CAMERA_REQUEST_CODE = 100
    private val RECORD_REQUEST_CODE = 101
    private val MIDIA_REQUEST_CODE = 102
    private var isFrontal = false
    private val executor = Executors.newSingleThreadExecutor()
    private var isRecording = false


    override fun onCreate(savedInstanceState: Bundle?) {
        setFullScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_spot_video)
        setUpColors()
        onCheckedPermission()
    }

    override fun onResume() {
        super.onResume()

        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            bindPreview(cameraProvider, CameraSelector.LENS_FACING_BACK)
        }, ContextCompat.getMainExecutor(this))
    }

    private fun setUpColors() {
        fabTakePicture.backgroundTintList = ColorStateList.valueOf(Color.parseColor(SpotVideoConfig.buttonColor))
        fabTakePicture.imageTintList = ColorStateList.valueOf(Color.WHITE)
        ivSwitchCamera.setColorFilter(Color.parseColor(SpotVideoConfig.buttonColor))
        tvCompressao.setTextColor(Color.parseColor(SpotVideoConfig.buttonColor))
        tvPorcent.setTextColor(Color.parseColor(SpotVideoConfig.buttonColor))
        progress_photo.progressTintList = ColorStateList.valueOf((Color.parseColor(SpotVideoConfig.buttonColor)))
        progress_photo.indeterminateTintList = ColorStateList.valueOf(Color.parseColor(SpotVideoConfig.buttonColor))
    }

    private fun onCheckedPermission() {
        if (ContextCompat.checkSelfPermission(
                this@SpotVideo,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Alerta.show(this@SpotVideo, getString(R.string.msg_atencao),
                resources.getString(R.string.msg_permissao_camera),
                resources.getString(R.string.btn_permitir),
                { dialog, which ->
                    ActivityCompat.requestPermissions(
                        this@SpotVideo,
                        arrayOf(Manifest.permission.CAMERA),
                        CAMERA_REQUEST_CODE
                    )
                },
                resources.getString(R.string.btn_cancelar),
                { dialog, which ->
                    onBackPressed()
                }, false
            )
            return
        }

        if (ContextCompat.checkSelfPermission(
                this@SpotVideo,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Alerta.show(this@SpotVideo, getString(R.string.msg_atencao),
                resources.getString(R.string.msg_permissao_gravar),
                resources.getString(R.string.btn_permitir),
                { dialog, which ->
                    ActivityCompat.requestPermissions(
                        this@SpotVideo,
                        arrayOf(Manifest.permission.RECORD_AUDIO),
                        RECORD_REQUEST_CODE
                    )
                },
                resources.getString(R.string.btn_cancelar),
                { dialog, which ->
                    onBackPressed()
                }, false
            )
            return
        }

        if (ContextCompat.checkSelfPermission(
                this@SpotVideo,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Alerta.show(
                this@SpotVideo, getString(R.string.msg_atencao),
                resources.getString(R.string.msg_permissao_arquivos),
                resources.getString(R.string.btn_permitir),
                { dialog, which ->
                    ActivityCompat.requestPermissions(
                        this@SpotVideo,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        MIDIA_REQUEST_CODE
                    )
                },
                resources.getString(R.string.btn_cancelar),
                { dialog, which ->
                    onBackPressed()
                }, false
            )
            return
        }

        onResume()
    }

    fun bindPreview(cameraProvider: ProcessCameraProvider, lensFacing : Int) {
        cameraProvider.unbindAll()
        val preview: Preview = Preview.Builder()
            .build()

        videoCapture = VideoCapture.Builder().build()


        val cameraSelector: CameraSelector = CameraSelector.Builder()
            .requireLensFacing(lensFacing)
            .build()

        preview.setSurfaceProvider(texture.surfaceProvider)

        val camera = cameraProvider.bindToLifecycle(
            this as LifecycleOwner,
            cameraSelector,
            preview,
            videoCapture
        )

        cameraExecutor = Executors.newSingleThreadExecutor()

        setupZoom(camera)
        setUpTakePicture()
        setUpReverseCamera()
    }

    private fun setUpReverseCamera() {
        if(!checkCameraFront(this))
            ivSwitchCamera.visibility = View.GONE
        else {
            ivSwitchCamera.setOnClickListener {
                if (progress_photo.visibility == View.GONE) {
                    isFrontal = if (!isFrontal) {
                        bindPreview(cameraProviderFuture.get(), CameraSelector.LENS_FACING_FRONT)
                        true
                    } else {
                        bindPreview(cameraProviderFuture.get(), CameraSelector.LENS_FACING_BACK)
                        false
                    }
                }
            }
        }
    }

    @SuppressLint("RestrictedApi", "MissingPermission")
    private fun setUpTakePicture() {
        val arquivo = File(SpotVideoConfig.tempFilePath)
        val outputFileOptions = VideoCapture.OutputFileOptions.Builder(arquivo)
            .build()

        fabTakePicture.setOnClickListener {
            if (progress_photo.visibility == View.GONE) {
                if (!isRecording) {
                    fabTakePicture.setImageResource(R.drawable.ic_stop)
                    progress_photo.visibility = View.GONE
                    setUpTimer(true)
                    isRecording = true

                    val handler = Handler(Looper.getMainLooper())
                    handler.postDelayed(
                        {
                            if (isRecording)
                                stop()
                        },
                        (SpotVideoConfig.timeLimit * 1000).toLong()
                    )

                    videoCapture!!.startRecording(outputFileOptions, executor,
                        object : VideoCapture.OnVideoSavedCallback {
                            override fun onVideoSaved(outputFileResults: VideoCapture.OutputFileResults) {
                                compressVideo(arquivo.absolutePath, SpotVideoConfig.tempFileCompressed)
                            }

                            override fun onError(
                                videoCaptureError: Int,
                                message: String,
                                cause: Throwable?
                            ) {
                                Handler(Looper.getMainLooper()).post {
                                    Alerta.show(
                                        this@SpotVideo,
                                        getString(R.string.video),
                                        getString(R.string.erro_video).replace("_ERROR", message),
                                        false
                                    )
                                }
                            }
                        })
                } else {
                    stop()
                }
            }
        }
    }

    private fun stop() {
        setUpTimer(false)
        fabTakePicture.setImageResource(R.drawable.ic_play)
        progress_photo.visibility = View.VISIBLE
        tvCompressao.visibility = View.VISIBLE
        isRecording = false
        stopRecording()
    }
    @SuppressLint("RestrictedApi")
    private fun stopRecording() {
        videoCapture!!.stopRecording()
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun setupZoom(camera: Camera) {

        val listener = object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
            override fun onScale(detector: ScaleGestureDetector): Boolean {
                val currentZoomRatio = camera.cameraInfo.zoomState.value?.zoomRatio ?: 0F

                val delta = detector.scaleFactor
                camera.cameraControl.setZoomRatio(currentZoomRatio * delta)
                return true
            }
        }

        val scaleGestureDetector = ScaleGestureDetector(this, listener)

        texture.setOnTouchListener { _, event ->
            scaleGestureDetector.onTouchEvent(event)
            return@setOnTouchListener true
        }

    }

    override fun onBackPressed() {
        if (progress_photo.visibility == View.GONE) {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
    }

    private fun setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onCheckedPermission()
    }

    fun checkCameraFront(context: Context): Boolean {
        return context.packageManager
            .hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)
    }

    private fun setUpTimer(isTiming: Boolean) {
        if (isTiming) {
            cmTimer.visibility = View.VISIBLE
            cmTimer.base = SystemClock.elapsedRealtime()
            cmTimer.start()
        } else {
            cmTimer.stop()
            cmTimer.visibility = View.GONE
        }
    }

    private fun compressVideo(videoPath: String, destinationFile: String) {
        VideoCompressor.start(
            context = applicationContext,
            srcUri = Uri.parse(videoPath),
            srcPath = videoPath,
            destPath = destinationFile,
            listener = object : CompressionListener {
                override fun onProgress(percent: Float) {
                    runOnUiThread {
                        tvPorcent.text = "${percent.toLong()}%"
                    }
                }

                override fun onStart() {
                    progress_photo.visibility = View.VISIBLE
                    tvCompressao.visibility = View.VISIBLE
                    tvPorcent.visibility = View.VISIBLE
                    tvPorcent.text = "0%"
                }

                override fun onSuccess() {
                    progress_photo.visibility = View.GONE
                    tvCompressao.visibility = View.GONE

                    val arquivoAntigo = File(videoPath)
                    arquivoAntigo.delete()

                    val intent = Intent()
                    intent.putExtra("arquivo", destinationFile)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }

                override fun onFailure(failureMessage: String) {
                    Alerta.show(applicationContext, getString(R.string.msg_atencao), getString(R.string.erro_video).replace("_ERROR", failureMessage), false)
                }

                override fun onCancelled() {
                    // On Cancelled
                }

            }, quality = VideoQuality.MEDIUM,
            isMinBitRateEnabled = false,
            keepOriginalResolution = false)
    }
}